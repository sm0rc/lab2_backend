from setup import db


class Car(db.Model):
    __tablename__ = "car"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    brand = db.Column(db.String(150), nullable=False, unique=False)
    license_plate_number = db.Column(db.String(9), nullable=False)

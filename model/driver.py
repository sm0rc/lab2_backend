from setup import db


class Driver(db.Model):
    __tablename__ = "driver"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(150), nullable=False, unique=False)
    age = db.Column(db.Integer, nullable=False)

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app(config_cls: type):
    app = Flask(__name__)
    from flask_cors import CORS
    CORS(app)
    app.config.from_object(config_cls)

    db.init_app(app)

    with app.app_context():
        from controller import car_routes, driver_routes
        app.register_blueprint(car_routes.cars)
        app.register_blueprint(driver_routes.drivers)

    return app

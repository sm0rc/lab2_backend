from flask import Blueprint, request, jsonify
from setup import db
from model.car import Car
from model.driver import Driver
from cfg.json_serializer import DbModelRelationshipSerializer
import json
from sqlalchemy.exc import IntegrityError

cars = Blueprint('cars', __name__, url_prefix='/cars')

@cars.get('')
def get_all_cars():
    all_cars = Car.query.all()
    return json.dumps(all_cars, cls=DbModelRelationshipSerializer)


@cars.get('/<int:car_id>')
def get_single_car(car_id):
    car = Car.query.filter_by(id=car_id).first()

    if not car:
        return jsonify("No car with such id"), 404

    return json.dumps(car, cls=DbModelRelationshipSerializer)


@cars.post('/add')
def add_car():
    car_data = request.get_json()
    new_car = Car(**car_data)

    db.session.add(new_car)
    db.session.commit()

    return jsonify('Added a new car')


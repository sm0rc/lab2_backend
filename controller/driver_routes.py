from flask import Blueprint, request, jsonify
from setup import db
from model.car import Car
from model.driver import Driver
from cfg.json_serializer import DbModelRelationshipSerializer
import json
from sqlalchemy.exc import IntegrityError

drivers = Blueprint('drivers', __name__, url_prefix='/drivers')

@drivers.get('')
def get_all_drivers():
    all_drivers = Driver.query.all()
    return json.dumps(all_drivers, cls=DbModelRelationshipSerializer)


@drivers.get('/<int:driver_id>')
def get_single_driver(driver_id):
    driver = Driver.query.filter_by(id=driver_id).first()

    if not driver:
        return jsonify("No driver with such id"), 404

    return json.dumps(driver, cls=DbModelRelationshipSerializer)


@drivers.post('/add')
def add_driver():
    driver_data = request.get_json()
    new_driver = Driver(**driver_data)

    db.session.add(new_driver)
    db.session.commit()

    return jsonify('Added a new driver')

